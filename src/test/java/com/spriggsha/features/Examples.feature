@example
Feature: Testing for Visual Regression Example

  @grid
  Scenario Outline: Test yahoo main page in multiple browsers
    Given I am testing a <description>
    And I am using a <typeOfBrowser> browser
    And I open a browser window at <width> px wide
    And I archive any existing test images
    When I navigate to url <url>
    And I take a screenshot and save it
    And I crop out the css element <cssSelector>
    Then I compare the screenshot against any old screenshots

  @chrome
    Examples:
      | description | typeOfBrowser | width | url                   | cssSelector          |
      | yahoo      | chrome        | 500   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | chrome        | 800   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | chrome        | 1200  | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | chrome        | 1600  | http://www.yahoo.com | div.sticky-outer-wrapper |

  @ie8
    Examples:
      | description | typeOfBrowser | width | url                   | cssSelector          |
      | yahoo      | ie8           | 500   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | ie8           | 800   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | ie8           | 1200  | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | ie8           | 1600  | http://www.yahoo.com | div.sticky-outer-wrapper |

  @ie9
    Examples:
      | description | typeOfBrowser | width | url                   | cssSelector          |
      | yahoo      | ie9           | 500   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | ie9           | 800   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | ie9           | 1200  | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | ie9           | 1600  | http://www.yahoo.com | div.sticky-outer-wrapper |

  @firefox
    Examples:
      | description | typeOfBrowser | width | url                   | cssSelector          |
      | yahoo      | firefox       | 500   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | firefox       | 800   | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | firefox       | 1200  | http://www.yahoo.com | div.sticky-outer-wrapper |
      | yahoo      | firefox       | 1600  | http://www.yahoo.com | div.sticky-outer-wrapper |


  @local
  Scenario Outline: Test yahoo main page in multiple browsers
    Given I am testing a <description>
    And I am using a <typeOfBrowser> browser
    And I open a browser window at <width> px wide
    And I archive any existing test images
    When I navigate to url <url>
    And I take a screenshot and save it
    Then I compare the screenshot against any old screenshots

  @chrome
    Examples:
      | description | typeOfBrowser | width | url                   |
      | yahoo      | local_chrome  | 500   | http://www.yahoo.com |
      | yahoo      | local_chrome  | 800   | http://www.yahoo.com |
      | yahoo      | local_chrome  | 1000  | http://www.yahoo.com |
      | yahoo      | local_chrome  | 1600  | http://www.yahoo.com |

#  @firefox
#    Examples:
#      | description | typeOfBrowser | width | url                   |
#      | yahoo      | local_firefox | 500   | http://www.yahoo.com |
#      | yahoo      | local_firefox | 800   | http://www.yahoo.com |
#      | yahoo      | local_firefox | 1000  | http://www.yahoo.com |
#      | yahoo      | local_firefox | 1600  | http://www.yahoo.com |
