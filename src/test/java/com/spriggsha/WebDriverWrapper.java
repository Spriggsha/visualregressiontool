package com.spriggsha;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverWrapper {
    WebDriver driver;
    String browser;
    URL hubURL;

    public WebDriverWrapper(String browser, String hubLocation) {
        this.browser = browser;
        DesiredCapabilities dc = new DesiredCapabilities();

        try {
            hubURL = new URL(hubLocation);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        switch (browser) {
            case "chrome":
                dc.setBrowserName("chrome");
                dc.setVersion("37");
                dc.setPlatform(Platform.WINDOWS);
                break;

            case "ie8":
                dc.setBrowserName("internet explorer");
                dc.setVersion("8");
                dc.setPlatform(Platform.WINDOWS);
                dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                break;

            case "ie9":
                dc.setBrowserName("internet explorer");
                dc.setVersion("9");
                dc.setPlatform(Platform.WINDOWS);
                dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                break;

            case "ie10":
                dc.setBrowserName("internet explorer");
                dc.setVersion("10");
                dc.setPlatform(Platform.WINDOWS);
                dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                break;

            case "ie11":
                dc.setBrowserName("internet explorer");
                dc.setVersion("11");
                dc.setPlatform(Platform.WINDOWS);
                dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                break;

            case "firefox":
//                FirefoxProfile profile = new FirefoxProfile();
//                profile.setPreference("focusmanager.testmode", true);
                dc = DesiredCapabilities.firefox();
//                dc.setCapability(FirefoxDriver.PROFILE, profile);
                dc.setBrowserName("firefox");
                dc.setVersion("32");
                dc.setPlatform(Platform.WINDOWS);
                try {
                    // To prevent port-locking issues common to firefox when multi-threading tests
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;

            case "safari7":
                dc.setBrowserName("safari");
                dc.setVersion("7");
                dc.setPlatform(Platform.ANY);
                break;

            case "android":
                dc.setCapability("device", "Android");
                dc.setCapability("app", "chrome");
                dc.setCapability("platform", "ANDROID");
                break;

            case "ios":
                dc.setBrowserName("iPhone5");
                dc.setCapability("version", "7.1");
                dc.setPlatform(Platform.MAC);
                break;

            case "phantom":
                dc.setBrowserName("phantomjs");
                dc.setPlatform(Platform.ANY);
                break;

            case "nojs":
                FirefoxProfile fp = new FirefoxProfile();
                fp.setPreference("javascript.enabled", false);
                dc.setBrowserName("firefox");
                dc.setVersion("32");
                dc.setPlatform(Platform.WINDOWS);
                dc.setCapability(FirefoxDriver.PROFILE, fp);
                break;

            case "local_chrome":
                System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
                driver = new ChromeDriver();
                break;

            case "local_firefox":
                driver = new FirefoxDriver();
                break;

        }

        if(!browser.contains("local_"))
            driver = new RemoteWebDriver(hubURL, dc);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getBrowser() {
        return browser;
    }

}
