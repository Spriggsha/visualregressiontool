package com.spriggsha.stepDefs;


import com.spriggsha.Screenshot;
import com.spriggsha.WebDriverWrapper;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.im4java.core.CommandException;
import org.im4java.core.IMOperation;
import org.im4java.core.ImageCommand;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.remote.Augmenter;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;

import static org.junit.Assert.assertTrue;

public class BrowserStepDefs {

    // Base Objects needed for all tests
    WebDriverWrapper webDriverWrapper;
    WebDriver driver;
    Screenshot screenshot;
    File oldScreenshotFile;
    File diffScreenshotFile;
    File screenshotFile;
    Double errorPixels;
    Double percentError = 0.0;
    byte[] screenshotByte;

    // Building this testlog String instead of println'ing, otherwise the parallel
    // nature of the framework gets all the test data jumbled in the output.
    String testlog = "---------------------------------------\n";

    // At what percent difference do we call the test a 'Fail'?
    Double errorThreshold = 1.5;

    // Specifies an ImageMagick 'Fuzz' percent, which allows for slight pixel differences
    // (useful for ignoring anti-aliasing differences).
    Double fuzzPercent = 20.0;

    // Location of Grid.  If not using a Grid, this is not necessary.
    String wdBrowserHub = "http://localhost:4444/wd/hub";


    // Instantiate the Screenshot Object
    @Given("^I am testing a (.*)$")
    public void I_am_testing_a_description(String description) throws Throwable {
        screenshot = new Screenshot();
        screenshot.setDescription(description);
    }

    // Figure out how your specific WebDriver is going to work.
    // In this instance, we have a separate WebDriverWrapper Class which
    // configures a RemoteWebDriver according to what's available on our
    // Grid.
    @Given("^I am using a (.*) browser$")
    public void I_am_using_a_typeOfBrowser_browser(String browser) throws Throwable {
        webDriverWrapper = new  WebDriverWrapper(browser, wdBrowserHub);
        driver = webDriverWrapper.getDriver();
        screenshot.setBrowser(browser);
    }

    // Set width before actually opening the page
    // Also, now that we have all the needed information, we can
    // determine the appropriate filename for the Screenshot.
    @Given("^I open a browser window at (.*) px wide$")
    public void I_open_a_browser_window_at_width_px_wide(int width) throws Throwable {
        driver.manage().window().setSize(new Dimension(width, 1000));
        screenshot.setWidth(width);
        screenshot.determineFileNameFromData();
    }

    // If we've run this particular test before, we want to put that
    // image into an 'old' folder.
    // Also initializes the various Files we're going to need.
    @Given("^I archive any existing test images$")
    public void I_archive_any_existing_test_images() throws Throwable {
        screenshotFile = new File("./target/shots/" + screenshot.getFileName());
        oldScreenshotFile = new File("./target/shots/old/"+screenshot.getFileName());
        diffScreenshotFile = new File("./target/shots/" + screenshot.getBrowser() + "_" + screenshot.getWidth() + "_" + screenshot.getDescription() + "_diff.png");
        File archivedDir = new File("./target/shots/old");
        Path oldPath = screenshotFile.toPath();
        if(screenshotFile.exists()){
            if(!archivedDir.exists()){
                archivedDir.mkdir();
            }
            Files.move(oldPath,oldScreenshotFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
        }
        if(diffScreenshotFile.exists()){
            diffScreenshotFile.delete();
        }
    }

    // Simple URL navigation
    @When("^I navigate to url (.*)$")
    public void I_navigate_to_url(String url) throws Throwable {
        driver.get(url);
        waitForPageToLoad();
    }

    // Two screenshots here.  One is going to be saved for comparison.
    // The other 'screenshotByte' is specifically for the Cucumber report.
    @When("^I take a screenshot and save it$")
    public void I_take_a_screenshot_and_save_it() throws Throwable {
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        screenshotByte = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.BYTES);
        File fullScreenShot = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(fullScreenShot, screenshotFile);
        } catch (IOException e) {
            testlog +=  "Unable to save the screenshot\n";
            e.printStackTrace();
        }
    }

    // For the parts of the page that are never, ever, ever going to match from
    // screenshot to screenshot.
    @When("^I crop out the css element (.*)$")
    public void I_crop_out_the_css_element_cssSelector(String css) throws Throwable {
        // get the coordinates of the element corners
        Point topLeft = driver.findElement(By.cssSelector(css)).getLocation();
        Dimension dimension = driver.findElement(By.cssSelector(css)).getSize();

        int topLeftX = topLeft.getX();
        int topLeftY = topLeft.getY();
        int width = dimension.getWidth();
        int height = dimension.getHeight();

        BufferedImage img = ImageIO.read(screenshotFile);
        Graphics2D graph = img.createGraphics();
        graph.setColor(Color.BLACK);
        graph.fill(new Rectangle(topLeftX,topLeftY, width, height ));
        graph.dispose();

        ImageIO.write(img, "png", screenshotFile);


    }


   // If the test has never seen a particular screenshot before, it fails, forcing the tester
    // to verify that the given image is valid.
    @Then("^I compare the screenshot against any old screenshots$")
    public void I_compare_the_screenshot_against_any_old_screenshots() throws Throwable {
        assertTrue("\n\n" +
                   "\n-------------------------------------------------------------" +
                   "\nI don't appear to have had an existing screenshot to compare:" +
                "\nBrowser:  " + screenshot.getBrowser() +
                "\nWidth:    " + screenshot.getWidth() +
                "\nDescription " + screenshot.getDescription() +
                "\nagainst. " +
                "\nCheck that the file at " + screenshotFile.getPath() + " is correct" +
                "\n-------------------------------------------------------------\n\n" ,

                oldScreenshotFile.exists());

        // ImageMagick comparison via the im4java API
        ImageCommand compare = new ImageCommand();
        compare.setCommand("compare");
        IMOperation op = new IMOperation();
        op.fuzz(fuzzPercent);
        op.metric("AE");
        op.dissimilarityThreshold(1.0);
        op.highlightColor("blue");
        op.addImage(screenshotFile.getCanonicalPath());
        op.addImage(oldScreenshotFile.getCanonicalPath());
        op.addImage(diffScreenshotFile.getCanonicalPath());
        try {
            compare.run(op);
        }catch(CommandException ce){
            if(ce.getReturnCode() == -1) {
                testlog += "Comparison Successful\n";
            }
        }

        // This is the output of the compare command, giving us the actual number of
        // 'different' pixels between the two images.
        errorPixels = Double.valueOf(compare.getErrorText().get(0));

        // 'different' pixels, divided by the total number of pixels, gives the
        // error percentage.
        if(diffScreenshotFile.exists()){
            java.awt.Dimension imageDimension = getImageSize(diffScreenshotFile);
            Double imageSize = imageDimension.getHeight() * imageDimension.getHeight();
            percentError = errorPixels/imageSize * 100;
            testlog += "Error percentage = " + percentError + "%\n";
        }

        // Assert for pass/fail.  ErrorThreshold should be configured to reflect
        // your given project.
        assertTrue("Diff between old and new screenshot: " + percentError + "%",errorThreshold > percentError);
    }

    // Add data to the Cucumber report and clean up the WebDriver.
    @After
    public void cleanUp(Scenario scenario) throws IOException {
        scenario.write("Error Percentage for --" + screenshot.getDescription() + "_" + screenshot.getBrowser() + "_" + screenshot.getWidth() + "-- is : " + percentError);
        scenario.write("New Screenshot");
        scenario.embed(screenshotByte, "image/png");
        if(oldScreenshotFile.exists()) {
            scenario.write("Old Screenshot");
            scenario.embed(Files.readAllBytes(oldScreenshotFile.toPath()), "image/png");
        }
        if(diffScreenshotFile.exists()) {
            scenario.write("Difference");
            scenario.embed(Files.readAllBytes(diffScreenshotFile.toPath()), "image/png");
        }
        driver.quit();
        webDriverWrapper = null;
        testlog += "Name: " + scenario.getName() +"\n";
        testlog += "Id: " + scenario.getId() + "\n";
        testlog += "Tags: " + scenario.getSourceTagNames().toString() + "\n";
        testlog += "Status: " + scenario.getStatus() + "\n";
        testlog += "---------------------------------------\n";
        System.out.println(testlog);
    }

    private static java.awt.Dimension getImageSize(File resourceFile) throws IOException {
        ImageInputStream in = null;
        try {
            in = ImageIO.createImageInputStream(new FileInputStream(resourceFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
            if (readers.hasNext()) {
                ImageReader reader = readers.next();
                try {
                    reader.setInput(in);
                    return new java.awt.Dimension(reader.getWidth(0), reader.getHeight(0));
                } finally {
                    reader.dispose();
                }
            }
        } finally {
            if (in != null) in.close();
        }

        return null;

    }



    public void waitForPageToLoad() throws InterruptedException {

        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            for (int x = 0; x < 200; x++) {
                String state = (String) js.executeScript("return document.readyState");
                if (state.equals("complete"))
                    break;
                Thread.sleep(10);
            }
            Thread.sleep(5000);
         // If there's not javascript available on the browser, just wait 5 seconds.
        } catch (Exception e) {
            Thread.sleep(5000);
        }

    }
}
