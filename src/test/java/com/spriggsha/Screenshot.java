package com.spriggsha;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Screenshot {
    String browser;
    int width;
    String description;
    String fileName;
    Date creationDate;
    String timeStampString;

    public Screenshot() {
        creationDate = Calendar.getInstance().getTime();
        timeStampString = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(creationDate);
    }

    // all these files should have the format {browser}_{width}_{description}.png
    public Screenshot(String filename) throws IOException {
        String[] stuff = filename.split("_");
        if (stuff.length != 3)
            throw new IOException("There was an error reading the existing screenshots." +
                    "\nAll these files should have the format {browser}_{width}_{description}.png");
        this.browser = stuff[1];
        this.width = Integer.parseInt(stuff[0]);
        this.description = stuff[2];
        creationDate = Calendar.getInstance().getTime();
        timeStampString = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(creationDate);
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "com.spriggsha.Screenshot{" +
                "browser='" + browser + '\'' +
                ", width=" + width +
                ", description='" + description + '\'' +
                ", fileLocation='" + fileName + '\'' +
                '}';
    }

    public void determineFileNameFromData() {
        this.fileName = this.browser + "_" + this.width + "_" + this.description + ".png";
    }
}
