#Visual Regression Tool

##Intro
The Visual Regression Tool is designed to allow users to compare an in-browser screenshot of a build to the same screenshot of the previous build, check the difference between them, and pass/fail the test based on a threshold set by the tester.

The tool uses 

* *Selenium Webdriver* to take the screenshots, 

* *im4java* (Java API for ImageMagick) to make the comparisons, and 

* *Cucumber-JVM* to run the tests and report.  

##Setup

###If using a Selenium Grid
The instantiation of the WebDriver object is accomplished through passing the required browser string to the WebDriverWrapper class.  Just replace the wdBrowserHub String with the location of your Grid: 

`String wdBrowserHub = "http://www.github.com";`

Then, in the webDriverWrapper class, define what kinds of DesiredCapabilities are appropriate for your particular Grid.

###If NOT using a Selenium Grid
By default, the WebDriver wrapper class is capable of launching local copies of Chrome and Firefox.  In your feature file, replace the <browser> variable with **local_chrome** or **local_firefox**.


###Additional Steps
INSTALL ImageMagick.  im4java is just a wrapper for the ImageMagick executables.  

* On Windows, download and run the installer from the ImageMagick site.

* On Mac, you can use Homebrew (`brew install imagemagick`)

Take a look at the **Examples.feature** file, and you'll see the kinds of Steps the application is designed to handle, by default.  One Step of note is the `When I crop out the css element <cssSelector>` step.  It's for blacking out those parts of your page which are likely to change from screenshot to screenshot (such as rotating ads) so they don't interfere with the comparison and create false-positives.  The rest of the examples should be fairly straightforward.

##Running the Tests
We're using Maven to run the tests.  In addition to the mvn command, you'll also need to set

* *Cucumber Options:* Exactly which @tagged features do you want to run / not run.

* *Threads:* The tool is designed to allow tests to run in parallel.

Here's a typical command: 

`mvn install "-Dcucumber.options= --tags @chrome --tags ~@wip,~@notthreadsafe" -Dthreads=2`.  

It gets broken down like this:

`mvn install`:  kicks off the tests.  We're using install instead of test because there are some pre- and
post-processing tasks we need to accomplish in other phases, and it all seemed to fit together best like
this.

`"-Dcucumber.options= --tags @chrome --tags ~@wip,~@notthreadsafe"`: Cucumber tags to run or not run.
Separate `--tags` sections are treated as logical AND.  You can also put commas between the elements of a 
tag to make logical OR.  So, the previous command is, *"Run all features with the @chrome tag AND do not run 
any features which have a @wip tag OR a @notthreadsafe tag"* (tilde is a NOT modifier).

So, if I look at the **Example.feature** file, and I decide I want to run Chrome tests, but only those Chrome tests which run locally (not on the Grid), and nothing that's still work-in-progress (@wip) the command would be:

`"-Dcucumber.options= --tags @local --tags @chrome --tags ~@wip"` 

`-Dthreads=2`: How many tests you want to run at once.  Simple enough.

##Results

By default, the pass/fail threshold for differences between build *n* and build *n-1* is set at 1.5%, but you can raise or lower
that value depending on your specific needs.  Just replace the **errorThreshold** variable in the **BrowserStepDefs.java** class.

The results come in a cucumber report which will be located at **/target/cucumber/index.html**.  Tests will fail if the recorded difference is higher than the defined threshold.

If a test runs, and there is no previous screenshot to compare against, it will fail with a message to check the screenshot just taken to make sure there are no issues with it.


##Notes

Be careful about using `mvn clean...` when running tests. A previous build's screenshots are kept in the /target/shots
folder, and if they are deleted during the test process, your tests are going to fail every time.

On some browsers, you're going to run into anti-aliasing problems, where the small differences in the way the text
of your page was rendered between tests is going to create noise and get in the way of your test results.  There may
be ways of accounting for that in the future, but for now, your best bet is to raise the fuzzPercent value `Double fuzzPercent = 20;` until
you start getting test results that work for you.  "Fuzzing" the results means telling ImageMagick to allow for greater color differences
between pixels before calling them "different".  A value of 70 seems to get rid of all false-positives due to
anti-aliasing, but if you set your fuzz that high, make sure you set your errorThreshold low (zero would be best) so
you don't lose out on valuable data while you're trying to account for false-positives.

If you find yourself wanting to move the locations of files around for any reason, make sure to take
a good look at `src/main/java/com/spriggsha/FeatureBuilder.java` and `src/main/java/com/spriggsha/ReportMerger.java` first.
These are two small programs which run before and after the actual tests, and they are the reason we 
can run the tests in parallel.


**FeatureBuilder.java** takes all the Scenarios that you selected in your mvn command and splits them 
out into individual feature files and Junit-style test-runner files, so that the Maven Surefire 
plugin can then assign each of them its own thread.

**ReportMerger.java** is necessary so that we can take the cucumber reports generated by each of the
separate threads and combine them all back into a single cucumber report.

They kind of like knowing where things are, so if you modify any file/folder locations, make sure these two classes
know about the changes.

After your test runs, you might find a bunch of files you weren't expecging in `src/test/java/com/spriggsha/resources/tempFeatures`
and `src/test/java/com/spriggsha/resources/tempRunners`.  These are evidence that FeatureBuilder has done its job.  They'll
be deleted and re-created on the next test run, so you don't need to do anything with them (other than not
commit them to git).




















